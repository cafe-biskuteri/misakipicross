#!/usr/bin/env python3

from PIL import Image

spritesheet = Image.open("misaki_mincho.png");

reprs = [];
for r in range(16, 84 + 1):
    for c in range(1, 94 + 1):
        x, y = (c - 1) * 8, (r - 1) * 8;
        bounds = (x, y, x + 7, y + 7);
        glyph = spritesheet.crop(bounds);
        pixels = glyph.load();

        number = 0;
        for y in range(7):
            for x in range(7):
                number <<= 1;
                number |= pixels[x, y] == 0;

        #repr = hex(number)[2:];
        #repr = repr.ljust(16, "0");
        #print(repr);
        #print(number);
        reprs.append(number);

print(reprs);

