/* copyright

This file is part of MisakiPicross.
Copyright (C) 2024 Usawashi Sugihara

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

copyright */

var canvas = null;
var debug = null;

var ys = 50, y1 = 200, ye = 500;
var xs = 50, x1 = 200, xe = 500;
var rh = (ye - y1) / 7, cw = (xe - x1) / 7;

var x2 = 800 - (800 - xe) / 2;

var playerFills = BigInt(0);
var playerFlags = BigInt(0);
var puzzle = BigInt(0);
var puzzleChar = null;

function log(message)
{
	debug.textContent += message;
}

function randomFrom(array) {
	return array[Math.floor(Math.random() * array.length)];
}

async function pickPuzzle(spritesheet)
{
	let idiv = (n, d) => Math.floor(n / d);
	puzzle = null;
	while (puzzle == null)
	{
		let pick = randomFrom(gameData);
		if (pick == 0) continue;
		puzzle = BigInt(pick);
		let puzzleO = gameData.findIndex(n => n == puzzle);
		let puzzleC = puzzleO % 94;
		let puzzleR = idiv(puzzleO, 94);
		puzzleChar = gameChars[puzzleR][puzzleC];
	}
}

function render()
{
	var g = canvas.getContext("2d");
	
	g.fillStyle = "white";
	g.clearRect(0, 0, 800, 600);
	g.fillRect(0, 0, 800, 600);

	// Fill board background.
	g.fillStyle = "lightblue";
	g.fillRect(x1, ys, xe - x1, y1 - ys);
	g.fillRect(xs, y1, x1 - xs, ye - y1);
	g.fillStyle = "aliceblue";
	g.fillRect(x1, y1, xe - x1, ye - y1);

	// Draw player markings on the grid.
	let idiv = (n, d) => Math.floor(n / d);
	for (let o = 0; o < 49; ++o)
	{
		let ro = 49 - 1 - o;
		let x = x1 + (ro % 7) * cw;
		let y = y1 + idiv(ro, 7) * rh;

		let mask = BigInt(1) << BigInt(o);
		let glyphSet = (puzzle & mask) != 0;
		let playerFill = (playerFills & mask) != 0;
		let playerFlag = (playerFlags & mask) != 0;

		/*
		if (glyphSet)
		{
			g.fillStyle = "lightgreen";
			g.fillRect(x, y, cw, rh);
		}
		*/
		if (playerFill)
		{
			g.fillStyle = "black";
			g.fillRect(x, y, cw, rh);
		}
		if (playerFlag)
		{
			g.fillStyle = "black";
			g.beginPath();
			g.ellipse(
				x + cw * 1/2,
				y + cw * 1/2,
				cw * 3/8, cw * 3/8,
				0, 0, Math.PI * 2);
			g.fill();
		}
	}

	// Draw board lines.
	g.beginPath();
	g.indieLine = function(xs, ys, xe, ye) {
		g.moveTo(xs, ys); g.lineTo(xe, ye);
	}
	for (let x = x1; x <= xe; x += cw) g.indieLine(x, ys, x, ye);
	g.indieLine(x1, ys, xe, ys);
	for (let y = y1; y <= ye; y += rh) g.indieLine(xs, y, xe, y);
	g.indieLine(xs, y1, xs, ye);
	g.stroke();

	// Draw labels.
	g.font = "20px MotoyaLMaru";
	g.textBaseline = "middle";
	g.fillStyle = "black";
	for (let r = 1; r <= 7; ++r)
	{
		let digits = [];
		let wasUnset = true;
		for (let c = 1; c <= 7; ++c)
		{
			let o = (7 - r) * 7 + (7 - c);
			let mask = BigInt(1) << BigInt(o);
			let set = (puzzle & mask) != 0;

			if (set)
			{
				if (wasUnset) digits.push(1);
				else ++digits[digits.length - 1];
				wasUnset = false;
			}
			else wasUnset = true;
		}

		let label = digits.join(" ");
		let metrics = g.measureText(label);
		let y = y1 + rh * (r - 1) + rh * 1/2;
		let x = x1 - cw * 1/4 - metrics.width;
		g.fillText(label, x, y);
	}
	for (let c = 1; c <= 7; ++c)
	{
		let digits = [];
		let wasUnset = true;
		for (let r = 1; r <= 7; ++r)
		{
			let o = (7 - r) * 7 + (7 - c);
			let mask = BigInt(1) << BigInt(o);
			let set = (puzzle & mask) != 0;

			if (set)
			{
				if (wasUnset) digits.push(1);
				else ++digits[digits.length - 1];
				wasUnset = false;
			}
			else wasUnset = true;
		}

		let label = digits.join(" ");
		let metrics = g.measureText(label);
		let x = x1 + cw * (c - 1) + cw * 1/2;
		let y = y1 - rh * 1/4 - metrics.width;
		g.translate(x, y);
		g.rotate(Math.PI / 2);
		g.fillText(label, 0, 0);
		g.rotate(-Math.PI / 2);
		g.translate(-x, -y);
	}

	// Draw victory.
	let victory = playerFills == puzzle;
	if (victory)
	{
		g.textBaseline = "alphabetic";

		let text = "You've won~!";
		let xo = -g.measureText(text).width / 2;
		let yo = -rh * 1/4;
		g.fillText(text, x2 + xo, y1 + yo);

		let w = 5 * cw, w2 = w / 2;
		g.strokeRect(x2 - w2, y1, w, w);

		g.font = "180px cwTexKai";
		g.textBaseline = "middle";
		text = puzzleChar;
		xo = -g.measureText(text).width / 2;
		g.fillText(text, x2 + xo, y1 + w2);
	}
}

function mouseAction(eM, or)
{
	let canvasRect = canvas.getBoundingClientRect();
	let x = eM.clientX - canvasRect.left;
	let y = eM.clientY - canvasRect.top;

	let c = 1 + Math.floor((x - x1) / cw);
	let r = 1 + Math.floor((y - y1) / rh);
	if (c < 1 || c > 7) return;
	if (r < 1 || r > 7) return;
	if (eM.button != 0) return;

	let action = "fill";
	if (eM.shiftKey) action = "flag";

	if (action == "fill")
	{
		let o = (7 - r) * 7 + (7 - c);
		let mask = BigInt(1) << BigInt(o);
		if (or) playerFills |= mask;
		else playerFills ^= mask;
	}
	else if (action == "flag")
	{
		let o = (7 - r) * 7 + (7 - c);
		let mask = BigInt(1) << BigInt(o);
		if (or) playerFlags |= mask;
		else playerFlags ^= mask;
	}

	let victory = playerFills == puzzle;
	if (victory)
	{
		log("Won the puzzle for " + puzzleChar + ".\n");
	}

	render();
}

var mouseHandler = {
	dragging: false,
	clicked: function(eM) {
		mouseAction(eM, false);
	},
	/*
	down: function(eM) {
		mouseHandler.dragging = true;
	},
	up: function(eM) {
		mouseHandler.dragging = false;
	},
	move: function(eM) {
		if (!mouseHandler.dragging) return;
		mouseAction(eM, true);
	}
	This has a bug where the last tile out gets a click
	for some reason. And, ideally we have it spread out
	the new state of the first tile that was affected.
	That takes some code, so switching off for now.
	*/
}

async function startupGame()
{	
	canvas = document.getElementById("game");
	debug = document.getElementById("debug");

	log("Selecting a puzzle glyph..\n");
	await pickPuzzle();
	log("Rendering game board..\n");
	await render();
	log("Enabling controls..\n");
	canvas.addEventListener("click", mouseHandler.clicked);
	canvas.addEventListener("mousedown", mouseHandler.down);
	canvas.addEventListener("mouseup", mouseHandler.up);
	canvas.addEventListener("mousemove", mouseHandler.move);
	log("Done.\n");
}

window.addEventListener("load", startupGame);
